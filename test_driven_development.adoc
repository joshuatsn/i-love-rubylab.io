== Test Driven Development

Imagine you are in circus, a beautiful girl is performing in trapeze, she misses the grip and falls down, would you expect a safety net to be there and catch her? You must be insane for you to say no. In a similar fashion, lets say that you are developing a software, you make a blunder that would cost a lot to people using it, wouldn't it be a good thing to have checks and balances so that the blunder is known even before the software is shipped?

Welcome to Test Driven Development. In this methodology we write tests firsts, and then we write just enough code to to satisfy the test. By following this methodology, I am able to code with supreme confidence, and am able to change the code and make it better (aka refactoring) knowing that there is a safety net to catch me in case I have done something wrong.

Lets take a imaginary scenario. You are tasked with coding a chat bot, the initial requirements as shown

* There must be a chat bot
* One must be able to set its age and name
* Its greeting must say "Hello I am <name> and my age is <age>. Nice to meet you!"

Usually the requirements won’t be as precise like the one shown above, but as a programmer, one should be able to think it out. Now we rather than writing code to solve the task, we start to write a test file, lets name it as test_chat_bot.rb and put the requirements in it as shown below:

[source, ruby]
----
# test_chat_bot.rb

# There must be a chat bot

# One must be able to set its age

# One must be able to set its name

# Its greeting must say "Hello I am <name> and my age is <age>.
# Nice to meet you!"
----

Now the requirements are put as words in our program, we need to translate it into Ruby. Almost all programming languages have a test framework built into them, Ruby too has one and its called Minitestfootnote:[https://ruby-doc.org/stdlib-2.0.0/libdoc/minitest/rdoc/MiniTest.html]. We will be using it here. To include Minitest we add the line shown highlighted below

[source, ruby]
----
# test_chat_bot.rb

require "minitest/autorun"

# There must be a chat bot

# One must be able to set its age

# One must be able to set its name

# Its greeting must say "Hello I am <name> and my age is <age>.
# Nice to meet you!"
----

Now we have included Minitest, lets now write a test class as shown below

[source, ruby]
----
# test_chat_bot.rb

require "minitest/autorun"

class TestChatBot < Minitest::Test
  # There must be a chat bot

  # One must be able to set its age

  # One must be able to set its name

  # Its greeting must say "Hello I am <name> and my age is <age>.
  # Nice to meet you!"
end
----

Having done whats shown above, lets now code for the first test case, take a look at the code below

[source, ruby]
----
class TestChatBot < Minitest::Test
  # There must be a chat bot
  def test_there_must_be_a_chat_bot
    assert_kind_of ChatBot, ChatBot.new
  end

  # One must be able to set its age

  # One must be able to set its name

  # Its greeting must say "Hello I am <name> and my age is <age>.
  # Nice to meet you!"
end
----

There is a lot going on in the above code, first we have written a test function by writing

[source, ruby]
----
  def test_there_must_be_a_chat_bot
  end
----

Note how this function starts with a `test_`, this is essential for it to be identified as a test. Next we must test some thing in this function. We can use the instance of an `ChatBot` class only if the `ChatBot` class exists, so we try creating a new `ChatBot` instance and check if its class is `ChatBot`, in the following piece of highlighted code

[source, ruby]
----
  def test_there_must_be_a_chat_bot
    assert_kind_of ChatBot, ChatBot.new
  end
----

Look at this thing `assert_kind_of`, if you are wondering what these are let me explain. These are called assertions. You can see what assertions are there here http://ruby-doc.org/stdlib-2.0.0/libdoc/minitest/rdoc/MiniTest/Assertions.html#method-i-assert_respond_to.

Assertions are functions that verify weather some thing expected is happening, if that happens, it means that test has passed, else it has failed.

Now lets run the test file test_chat_bot.rb

----
$ ruby test_chat_bot.rb
Run options: --seed 53866

# Running:

E

Finished in 0.000875s, 1142.2906 runs/s, 0.0000 assertions/s.

  1) Error:
TestChatBot#test_there_must_be_a_chat_bot:
NameError: uninitialized constant TestChatBot::ChatBot
    test_chat_bot.rb:9:in `test_there_must_be_a_chat_bot'

1 runs, 0 assertions, 0 failures, 1 errors, 0 skips
----

So we get the above output that says that no constant called `ChatBot` exists. Very well, we haven’t defined what `ChatBot` is and so we will define it. In the test_chatbot.rb , we add the line `require_relative "chat_bot.rb"` as shown below

[source, ruby]
----
# test_chat_bot.rb

require "minitest/autorun"
require_relative "chat_bot.rb"

class TestChatBot < Minitest::Test
  # There must be a chat bot
  def test_there_must_be_a_chat_bot
    assert_kind_of ChatBot, ChatBot.new
  end

  # One must be able to set its age

  # One must be able to set its name

  # Its greeting must say "Hello I am <name> and my age is <age>.
  # Nice to meet you!"
end
----

And we create a new file called chat_bot.rb with the following content

[source, ruby]
----
# chat_bot.rb

class ChatBot
end
----

Now lets run the test.

----
$ ruby test_chat_bot.rb
Run options: --seed 19585

# Running:

.

Finished in 0.000720s, 1388.5244 runs/s, 1388.5244 assertions/s.

1 runs, 1 assertions, 0 failures, 0 errors, 0 skips
----

What have we done? We had a requirement, we wrote test that covered the requirement, we ran the test, it failed, to pass it we wrote just enough code to make it pass. Now imagine a scenario, you are in a project with 10 developers, one of them accidentally makes a mistake that will rename this `ChatBot` class, and your tests would catch it. In short if you write enough tests, you can make bugs popping up found early. It does not guarantee bug free code, but makes bugs popping up a lot more difficult. These tests will also make you confident to refactor code. Say you make a change, there is no need to fear that your change might cause an havoc, just run the tests once and you will get a report of what fails and passes.

Lets write another test, one should be able to give chat bot an `age`. So lets write a test where we can set its age and read it back. Look at the code in function `test_one_must_be_able_to_set_its_age` below

[source, ruby]
----
# test_chat_bot.rb

require "minitest/autorun"
require_relative "chat_bot.rb"

class TestChatBot < Minitest::Test
  # There must be a chat bot
  def test_there_must_be_a_chat_bot
    assert_kind_of ChatBot, ChatBot.new
  end

  # One must be able to set its age
  def test_one_must_be_able_to_set_its_age
    age = 21
    chat_bot = ChatBot.new
    chat_bot.age = age
    assert_equal age, chat_bot.age
  end

  # One must be able to set its name

  # Its greeting must say "Hello I am <name> and my age is <age>.
  # Nice to meet you!"
end
----

Look at the code above, look at this line `assert_equal age, chat_bot.age`, here we assert weather the chat_bot returns the set age.

----
$ ruby test_chat_bot.rb
Run options: --seed 59168

# Running:

.E

Finished in 0.000855s, 2338.4784 runs/s, 1169.2392 assertions/s.

  1) Error:
TestChatBot#test_one_must_be_able_to_set_its_age:
NoMethodError: undefined method `age=' for #<ChatBot:0x0000558b89da5380>
    test_chat_bot.rb:16:in `test_one_must_be_able_to_set_its_age'

2 runs, 1 assertions, 0 failures, 1 errors, 0 skips
----

If you see above test result, it says no method error, and says the function `age=` is missing, so lets fix it

[source, ruby]
----
# chat_bot.rb

class ChatBot
  attr_accessor :age
end
----

So we add above `attr_accessor :age` line, and now run the test and it passes as shown below

----
$ ruby test_chat_bot.rb
Run options: --seed 42767

# Running:

..

Finished in 0.000774s, 2583.4820 runs/s, 2583.4820 assertions/s.

2 runs, 2 assertions, 0 failures, 0 errors, 0 skips
----

Lets now do the same for name too, I won’t explain it as its the same for age, and I have skipped its explanation in this book. Let me talk about the final test. It must greet some one. For that we write a test as shown in function `test_greeting_message` below:

[source, ruby]
----
# test_chat_bot.rb

require "minitest/autorun"
require_relative "chat_bot.rb"

class TestChatBot < Minitest::Test
  # There must be a chat bot
  def test_there_must_be_a_chat_bot
    assert_kind_of ChatBot, ChatBot.new
  end

  # One must be able to set its age
  def test_one_must_be_able_to_set_its_age
    age = 21
    chat_bot = ChatBot.new
    chat_bot.age = age
    assert_equal age, chat_bot.age
  end

  # One must be able to set its name
  def test_one_must_be_able_to_set_its_name
    name = "Zigor"
    chat_bot = ChatBot.new
    chat_bot.name = name
    assert_equal name, chat_bot.name
  end

  # Its greeting must say "Hello I am <name> and my age is <age>.
  # Nice to meet you!"
  def test_greeting_message
    name = "Zigor"
    age = 21
    expected_message = "Hello I am #{name} and my age is #{age}. Nice to meet you!"
    chat_bot = ChatBot.new
    chat_bot.name = name
    chat_bot.age = age
    assert_equal expected_message, chat_bot.greeting_message
  end
end
----

Now we run it as you see it naturally fails as shown

----
$ ruby test_chat_bot.rb
Run options: --seed 8752

# Running:

.E..

Finished in 0.001075s, 3720.5045 runs/s, 2790.3784 assertions/s.

  1) Error:
TestChatBot#test_greeting_message:
NoMethodError: undefined method `greeting_message' for #<ChatBot:0x000055b4ae5a8620 @name="Zigor", @age=21>
    test_chat_bot.rb:39:in `test_greeting_message'

4 runs, 3 assertions, 0 failures, 1 errors, 0 skips
----

so we modify the file link:code/chat_bot.rb[chat_bot.rb] as shown

[source, ruby]
----
# chat_bot.rb

class ChatBot
  attr_accessor :age, :name

  def greeting_message
    "Hello I am #{name} and my age is #{age}. Nice to meet you!"
  end
end
----

now we run the tests, and it passes as shown:

----
$ ruby test_chat_bot.rb
Run options: --seed 16324

# Running:

....

Finished in 0.001149s, 3480.2007 runs/s, 3480.2007 assertions/s.

4 runs, 4 assertions, 0 failures, 0 errors, 0 skips
----

So now we have got an application and a safety net around it, hence its more bug proof.

