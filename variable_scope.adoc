== Variable Scope

We have seen about functions in the last section and we have seen about variables before. I think the time is right to type about variable scope. In this chapter we examine how long or how far a variable is valuable when its declared in a particular section of a program. Lets start with a example. Fire up your text editor, type the code  below (link:code/variable_scope.rb[variable_scope.rb]) and execute it.

[source, ruby]
----
include::code/variable_scope.rb[]
----

Output

----
variable_scope.rb:7:in `print_x': undefined local variable or method `x' for main:Object (NameError) 
	from variable_scope.rb:10 
----

Well you get an error. See that you have declared a variable by typing `x = 5`. In the function `print_x` you tell the ruby program to print out the variable x, but it throws a error. Look at the output, it says `undefined local variable or method `x' for main:Object (NameError) from variable_scope.rb:10`, well we have defined `x` and have assigned it to value 5 at the beginning, then how come Ruby throws the error? Well, we have defined `x` outside the function `print_x` hence `x` has no scope inside it, so we get an error.

A good programmer is the one who exploits the advantages provided by a programing language and who is smart enough to play by rules and limitations it imposes. It might look as a real handicap for a newbie that we are not able to access a variable we have assigned outside a function, but as your program and you become  mature programmer you will realize its blessing in disguise.

To learn more type the program below (variable_scope_1.rb) in your text editor and execute it.

[source, ruby]
----
include::code/variable_scope_1.rb[]
----

Output

----
3 
5
----

Take a careful look at the output. First in the program we declare a variable `x = 5` , then in function `print_x` we declare a variable `x = 3`. Note that the variable declared in function `print_x` is not the same one as thats been declared outside the function. Next we call upon the function `print_x` which prints the output as 3 which is expected since inside `print_x` we have written `puts x` after `x = 3`. Next statement is the hero here, (outside the function) we have written `puts x` after `print_x`, if you expected to print 3 then you are wrong. Here `x` is the `x` that we have declared it outside the function, here it will get printed as 5. This means that a variable declared inside the function has no scope outside it.

To know more and to convince ourself that variable declared inside a function has no scope outside it, we will try out another program. Type in the program link:code/variable_scope_2.rb[variable_scope_2.rb] into your text editor and execute it.

[source, ruby]
----
include::code/variable_scope_2.rb[]
----

Output

----
3 
variable_scope_2.rb:10: undefined local variable or method `y' for main:Object (NameError)
----

Here is how the program works or should I say here is how the program doesn't works as it throws an error. Take a look at the function `print_variable`, in it we have declared a variable called `y` using statement `y = 3` and told the ruby interpreter to print its value using statement `puts y`. So in the program when we call `print_variable` the `y` is declared and its value 3 is printed without a glitch. Next we say `puts y` outside the function `print_variable`, since `y` is only declared within the function, outside it the variable `y` doesn't exist and in technical terms it has no scope, so the Ruby interpreter throws an error. So we get the error message as follows:

----
variable_scope_2.rb:10: undefined local variable or method `y' for main:Object (NameError)
----

'''

Looks like Matz (the creator of Ruby) hasn't seen the movie 'Back to the future'. Lets see another program that proves that time travel isn't built into Ruby, type the program below (link:code/variable_scope_3.rb[variable_scope_3.rb]) into your text editor and execute it.

[source, ruby]
----
include::code/variable_scope_3.rb[]
----

Output

----
variable_scope_3.rb:4: undefined local variable or method `a' for main:Object (NameError) 
----

If you have anticipated right, the program throws out an error. We have given `puts a` before `a` has been declared. Ruby interpreter does not consider whats declared in future, so when `puts a` is encountered it, at that point of time `a` is undeclared, and hence an error is thrown. In other words scope of an variable starts only after it has been declared.

=== Global Variables

If you are a one who don't like the idea that variables declared outside a function cant be accessed from it, then Ruby provides a way to do it. There are special variables called global variables that can be accessed from any where. Global variables are preceded by a  dollar ($) sign. To know about global variables lets see an example. Type the program below (link:code/global_variables.rb[global_variables.rb]) and execute it.

[source, ruby]
----
include::code/global_variables.rb[]
----

Output

----
3
3
----

Having run it successfully lets see how it works. First we declare a global variable $x and assign it to the value 5 in the statement `$x = 5`. Next we define a function `print_x` in which we change the value of `$x` to 3 using statement `$x = 3`, then we print the value of `$x` using `puts $x`. So obviously we call `print_x` we get the output as 3. Next outside the function after calling `print_x`, we print the value of `$x` using `puts $x`. If you think it would print 5, then you are mistaken. Since `$x` can be accessed from any where, and we have called `print_x`, and in `print_x` we have changed the value of `$x` to 3, no matter what, even outside the scope of the function the value of `$x` will be changed.

Lets see another example to understand global variables better. Look at the example below (link:code/global_variables_1.rb[global_variables_1.rb]), type it in your text editor and execute it

[source, ruby]
----
include::code/global_variables_1.rb[]
----

here is how the output of the program looks like

----
5 
7 
3 
----

Lets see how the program works. At first we declare a global variable `$x` and assign it to value five using `$x = 5`, then we define a function called `print_x`  in which we just print out the value of `$x` using `puts $x` statement. While we call the first print_x statement, the value of `$x` is 5 and hence 5 gets printed. Next we change the value of `$x` to 7 in statement `$x = 7` and when we call `print_x`, the value of `$x` which is now 7 gets printed. Finally we set `$x` to 3 using `$x = 3`, when we call `print_x` for the final time 3 gets printed out.

This program proves that global variables can be manipulated from any where and these manipulated values can be accessed from any where.
Next arises a question weather global variable and local variable can have the same name. The answer is yes. Its because global variables start with a `$` sign and local variables start with a letter or underscore character, so ruby can clearly tell the difference between them. Lets see a program that proves this, read, learn type and execute the program given below (link:code/global_variables_2.rb[global_variables_2.rb]). Once you are done with it, we will see how it works.

[source, ruby]
----
include::code/global_variables_2.rb[]
----

Output

----
In print_x 
$x = 3 
x = 3 
Outside print_x 
$x = 3 
x = 5 
----

In the above program we declare two variables one global `$x` and assign it to value 5 and another local `x` and assign it to value 3 in the following lines

[source, ruby]
----
$x = 5
x = 5
----

Next we create a function `print_x` in which we change the value of `$x` to 3, since `$x` is global, the change is affected every where in the program, next we have  statement `x = 3`, this variable `x` is local one and is different from `x = 5` which we defined outside the function. Next we will tell the program to print the values of `$x` and local `x` using he following statements

[source, ruby]
----
puts "In print_x"
puts "$x = "+$x.to_s
puts "x = "+x.to_s
----

OK, when the program encounters the `print_x` call, we get the following output

----
In print_x 
$x = 3 
x = 3 
----

Note that `$x` is now 3 and local `x` is also 3. Now outside the function we print the values of `$x` and `x` using the following statements (last 3 lines of the program)

[source, ruby]
----
puts "Outside print_x"
puts "$x = "+$x.to_s
puts "x = "+x.to_s
----

When these statements are executed, we get the following output

----
Outside print_x 
$x = 3 
x = 5 
----

Here as `$x` has been assigned to 3, 3 is printed as its value. `x` over here remains 5 as here `x` refers to not the `x` thats defined inside `print_x`, but the one thats defined out of it.

