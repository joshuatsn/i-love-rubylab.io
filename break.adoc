=== break

Suppose you want to break away from loop, that is to stop executing it, you can use the `break` command. An example is given below. In the example we will break if the iterating variable `i` becomes 6. So numbers ranging only from 1 to 5 gets printed. When `i` becomes 6 the loop breaks or terminates

[source, ruby]
----
include::code/break.rb[]
----

When executed, the above program produces the following output

----
1, 2, 3, 4, 5,
----

