== Meta Programming

Meta Programming is a art of making programs write programs. That is in run time a program can modify itself depending on the situation. Lets see about it in this chapter.

=== Send

Ruby has got a powerful method called `send`. That is if a object `p` has got a method `name`, in ruby we can call it using `p.name` or there is another way to call it too. We call it using `p.send(:name)` or `p.send("name")`. Well whats the use of that? The use is this, you can determine what function to call from the user input or some other input you receive.

Lets see a basic example. Type the program link:code/send.rb[send.rb] below into a text editor and run it.

[source, ruby]
----
include::code/send.rb[]
----

Output

----
Hello I am Karthik
----

Well, as you see in the part of the code highlighted p.send(:speak) , we are calling the speak function of instance p of class Person using the send method. That's it for now about send. Get excited!!! Tweet that you are learning Metaprogramming and start bragging to your colleagues.

Well, hope you have bragged enough. Now lets look at a bit more practical example for this `send` function. Type in the example link:code/send_1.rb[send_1.rb] and execute it

[source, ruby]
----
include::code/send_1.rb[]
----

Output

----
Enter the subject who's mark you want to know: math
The mark in math is 100
----

So in the program, we have a class called `Student` and we create a student who's marks in math, science and other subjects are 100, 100 and zero. We ask the user to enter the subject who's mark needs to be known and get it into a variable named subject in these following statements

[source, ruby]
----
print "Enter the subject who's mark you want to know: "
subject = gets.chop
----

now see this line:

[source, ruby]
----
puts "The mark in #{subject} is #{s.send(subject)}"
----

Just notice the part `s.send(subject)`, we over here instead of using case or other if or conditions to check what the subject is and then call the suitable method according, we simply pass the user input to `s.send` and it calls the appropriate method and returns the value.

Don't you see a magic here?

=== Method Missing

Lets say that you have a class that has only certain methods, and if the programmer calls some other crazy method, and you want to capture it and see if it can still be served, you can use the `method_missing` method to capture the method and other stuff.

Lets see a program about method missing. Type in the program link:code/method_missing.rb[method_missing.rb] in your test editor and execute it

[source, ruby]
----
include::code/method_missing.rb[]
----

Output

----
You called: call_method
["boo", 5]
in block
in block
in block
in block
in block
in block
in block
----

Lets see how this program works, in the line `s = Something.new` we create a new instance variable `s` of `Something` class. Then in the next line we do this `s.call_method "boo", 5`, in this line we call a method called `call_method` on `s`, if you look at class `Something`, you will notice that there is no method or function called call_method in it, but the program does not throw out error, or exception or whatever.

Well, what happened? If you notice Something class, you would have seen a method named method_missing, it has been implemented as shown

[source, ruby]
----
def method_missing method, *args, &block
  puts "You called: #{method}"
  p args
  block.call 7
end
----

This method takes in three arguments, in our code we have named these arguments as `method`, `*args` and `&block`. The method takes in the method name which is the name of the method being called on object `s`, the `*args` takes attributes that are passed to the method, in our case its the `call_method` and attributes passed are `“boo”` and `5`. The `&block` takes in any block that is been passed. If you see the we call `call_method` on `s` below

[source, ruby]
----
s.call_method "boo", 5 do |x|
  x.times{ puts "in block" }
end
----

We are passing a block to the `call_method` function which is enclosed by `do` and `end`. Inside the block we take a variable `x` and do some operation with it. This entire block is captured by `&block`.

Finally we are printing the arguments passed using the statement `p args` (note that we are not using `*args` here) and we are calling the block with `block.call 7` (note that we use block and not &block here) in the method_missing definition. The value `7` gets passed to variable `x` in the block.

Now lets see how method missing could be used. Let say that we have a class called `Person` which has two attributes named `name` and `age`, see the code below and execute it

[source, ruby]
----
include::code/method_missing_in_action.rb[]
----

Output

----
Zigor is 67893 years old
----

In the code above see the highlighted line `puts "#{person.get_name} is #{person.get_age} years old"` we call the attributes not like person.name and `person.age`, instead we use `person.get_name` and `person.get_age`. Now there are no `get_name` and `get_age` methods in `Person` class, instead the code ends up here

[source, ruby]
----
def method_missing method_name
   method_name.to_s.match(/get_(\w+)/)
   eval("self.#{$1}")
end
----

In the method missing method. Look at the code, in this line `method_name.to_s.match(/get_(\w+)/)` we extract any method that is perpended with `get_`, then we call the extracted term in this statement `eval("self.#{$1}")`. If you can't understand these things, probably you must read <<Regular Expressions>> chapter.

Now how to make it useful practically, for example you can have a `get_db_<method name>` where you can get values from a database, or say `store_db_<method name>(values....)` , where you can capture it and store in in the database.

=== Define Method

This section we are going to see how to define methods inside a class. Type the program below and execute it

[source, ruby]
----
include::code/define_method.rb[]
----

Output

----
25
----

Okay, so you got 25 as the output. If you notice the program link:code/define_method.rb[define_method.rb] you would have noticed that in the lines above we are defining method named area using this awkward looking statements as shown below

[source, ruby]
----
define_method :area do |side|
  side * side
end
----

You may think why not we do it like this:

[source, ruby]
----
def area side
  side * side
end
----

Well, ya, but lets take a situation where we can dynamically define method.

[source, ruby]
----
include::code/define_method_dynamic.rb[]
----

Output

----
Zigor has written a book named I Love Ruby
----

So in the above program we have two highlighted parts the first one is `Book_hash = {author: "Zigor", title: "I Love Ruby", page: 95}`, over here its a constant assigned to a hash value. In real world it could be a variable loading some hash dynamically from  a file. And inside the class book you see these lines:

[source, ruby]
----
Book_hash.each do |key, value|
  define_method key do
    value
  end
end
----

Where we take in each hash value, and we define a method using its key and the method returns the value. So when we say `b = Book.new` , we now have already functions named `author`, `title` and `page` which returns `“Zigor”`, `“I Love Ruby”` and `95` respectively.

For this statement, the `puts "#{b.author} has written a book named #{b.title}"`, explains it.

=== Class Eval

Lets say that you have an instance object, you want to add some thing to its class, you can use a method called `class_eval`, lets see with an example. Type the program below in a text editor and execute it.

[source, ruby]
----
include::code/class_eval.rb[]
----

Output

----
I am a dog
----

Look at the code shown below. So you have the variable `dog` which is instance of `Animal`. Lets say all of a sudden in the program we decided to add a method called `say_something` to class of `dog` i.e to `Animal`, all we need to do is to write that method inside `class_eval` block as shown highlighted below

[source, ruby]
----
dog.class.class_eval do
  def say_something
    "I am a dog"
  end
end
----

In the above program we get class of `dog` using `dog.class`, and to it we call `class_eval`, which is followed by `do` `end` block inside which we define the code to be appended to class of the `dog`. Inside it we define the method `say_something`.

Now lets say we have another variable named `pig` thats' instance of `Animal` and we call `pig.say_something` it responds too! So we have modified the the class `Animal`.

=== Instance Eval

In `class_eval` we saw that we can add methods to a class that can be accessed by its instance. `instance_eval` is kind of opposite. No we won't be removing methods :D , but this adds class methods to the calling class. Lets see an example

[source, ruby]
----
include::code/instance_eval.rb[]
----

Output

----
I am Square class
----

In the above example, look at the following piece of code:

[source, ruby]
----
Square.instance_eval do
  def who_am_i
    puts "I am Square class"
  end
end
----

All we need to do is to call `instance_eval` on a class and inside a block we need to write the code that will become class method. We have defined a function `who_am_i`, its quiet equal like typing this

[source, ruby]
----
class Square
  def self.who_am_i
    puts "I am Square class"
  end
end
----

And when we call `Square.who_am_i`, the method faithfully responds.

