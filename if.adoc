=== if

The if keyword is used to execute a statement if a condition is satisfied. Take a look at the program below. Execute it. 

[source, ruby]
----
include::code/if.rb[]
----

This is how thee result would be if you give a name other than Zigor

----
Whats your name? 
Karthik 
Karthik is idiot
----

Take a look at the program. Take a look at the following line

[source, ruby]
----
puts "#{name} is genius" if name == "Zigor"
----

The program gets your name in variable called `name` . Now it checks if the name is Zigor in the code above (that is in the right side of the keyword `if`), if yes it executes the statement associated with it (that is the statement in the left side of keyword `if`), in this case it prints out that the particular name is genius. It then comes down to next statement

[source, ruby]
----
puts "#{name} is idiot" if name != "Zigor"
----

In this statement it checks if name is not Zigor, if yes it prints the name is idiot.

