*Need for Design Patterns*

When software began, it was small, computers were low powered and were used for very menial tasks compared to what its been used for now, people were happy with small programs which a single person or a closely knit group could maintain. But as computers became more powerful and computers became more complex and projects became vast, structuring of code became an important issue. That’s when design patterns came to light.

Most people reading this book would be a Ruby beginner or an intermediate, but you may need to work in real projects. Even if you have chosen Ruby for your personal project, it better to structure your code well, hence the need for design pattern becomes essential.

