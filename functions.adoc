== Functions

When you are using same piece of code many times, you can group them into a thing called function, you can call that grouped code any where in your program to do that particular task. Lets take a real world example, you goto the hotel and a waiter comes up to you, you order an fish fry and you get it. You are not bothered what happens after you order.

Once you have ordered the fry, there are lots of procedures that takes place. The waiter notes down your order, he goes up to the kitchen and places the order chit to the chef, the chef tells him that it would take so much time for the dish to get cooked. The waiter thinks how he could keep you from getting bored, he arrives at your table, recommends a starter and/or an appetizer, serves you a drink that would go well before you eat the dish, he pools the kitchen to see if the dish is ready. If the dish is ready and if you have finished your starter he serves it. If you haven't finished your starter, he tells the kitchen to keep the dish warm and waits for you to finish. Once he gets the dish to your table, he lays the plate containing the dish and cutlery.

All you have done is to order a fish fry and have blissfully ignored what is being functioned at the background. You gave some order (input) to a waiter and got a dish (output). What to do and not to is preprogrammed or trained into the waiters mind, according to his training, the waiter functions. 

Lets get started with functions in Ruby. We will be looking at a program in which we will be writing a function called `print_line`, which prints a line. Type the following program into your text editor and run it.

[source, ruby]
----
include::code/function.rb[]
----

Output

----
____________________ 
This program prints lines 
____________________ 
----

Lets analyze the program. Consider the following piece of code:

[source, ruby]
----
def print_line
	puts '_'*20
end
----

The `def` tells that you are defining a function. A function must have a name, the name follows immediately after `def` key word. In this case the name of the function is `print_line`. In it you can write what ever code you want. In this case we are creating a line with twenty underscore characters.

So we have created a function and have added code into it. All we need to do now is to call the function from our program. Its done by just typing the name of the function in the program as in code below

[source, ruby]
----
print_line
puts "This program prints lines"
print_line
----

As seen in the output, a line made up of twenty underscore characters gets printed above and below the string `“This program prints lines”`.

=== Argument Passing

Lets get more control on the functions in this section. Sometimes you don't goto a hotel and order a single dish, you can order how much ever or how less you want. If you go with friends you can order more servings, if you are alone, you will order less.  Why can't we do the same thing with `print_line` function? Why can't we vary its length, doing so will be a wonderful thing, we can print lines of length what ever we choose.

Take a look at code below, we have typed a thing called `length` after the function name, its called as argument. Like a function, an argument has an name, in this case we have named it `length`. We can pass any values to it to vary the `length` of the line printed. Type the code below and execute it

[source, ruby]
----
include::code/function_1.rb[]
----


You will get a design pattern as shown below

----
__________ 
____________________ 
______________________________ 
________________________________________ 
__________________________________________________ 
________________________________________ 
______________________________ 
____________________ 
__________ 
----

Take a look at the following code

[source, ruby]
----
10.step(50,10) do |x|
	print_line x
end

40.step(10,-10) do |x|
	print_line x
 end
----

We have used `step` loop to increment or decrement the value which we capture it into a variable called `x` which is inside the loop, we pass `x` to the `print_line` function by placing it after its call as highlighted above. So each time a line of varying length (determined by `x`) gets printed. The program is constructed in a way so that a pattern is generated.

=== Default Argument

In link:code/function_1.rb[function_1.rb] you have seen how to pass an argument to a function. What if suppose you fail to pass an argument? If you do so, an error will be generated which a good programmer will not desire that to happen. To prevent this and to make programming a bit easy its better to provide a default argument to a function. Note the code given below in link:code/function_default_argument.rb[function_default_argument.rb] 

[source, ruby]
----
include::code/function_default_argument.rb[]
----

Execute the program and observe the result
----
____________________ 
________________________________________ 
----

You can see in the program, in function `print_line` by giving `length = 20`  we have indicated that if no argument is passed the function must assume that value of `length` is 20 . If passed this value will be overridden with what ever value you pass. As you can see in the first function call, we simply call the function just by its name `print_line`, we don't bother to pass value for length to it, yet we see a line of length 20 units gets printed in the output.

=== Passing array to a function

Usually when you pass a variable to a function,  a copy of the variable is made. And if you make changes to the variable, the one that's outside the function is unaffected. So lets see what happens when we pass array to function. Type in the program below and run it.

[source, ruby]
----
include::code/array_passed_to_function.rb[]
----

Output

----
[1, 2, 3, 4, 5]
[1, 2, 3, 4, 5, 6]
----

If you are newcomer to programming, this might not be surprising, but when a variable is passed to a function, its value is not supposed to change. But in case of array, inside the function `array_changer`, we are adding an element to it and it changes.  Well this is a peculiar behavior of an array getting passed to a function.

To avoid such behavior try the program below

[source, ruby]
----
include::code/array_copy.rb[]
----

Output

----
[1, 2, 3, 4, 5]
[1, 2, 3, 4, 5]
----

Here the array does not get changed, look at the line `array_changer Marshal.load(Marshal.dump(some_array))`, this piece of code copies `some_array` to the function argument so that even when its changed inside the function, it does not get changed outside the function.

//// Write about functional programming here

=== Returning Values

We have till now seen function taking in arguments, we will now see that the function can return back values which can be used for some purpose. Lets now see the program link:/code/function_return.rb[function_return.rb], study the code, type it and execute it.

[source, ruby]
----
include::code/function_return.rb[]
----

Output

----
8
----

The output you get out after executing is 8. Note the method named addition in the above program. It accepts two arguments `x` and `y`, inside the method we declare a  variable called `sum` which is assigned to the addition of `x` with `y`. The next statement is the hero here, see that we have used a keyword `return`, this returns the value out of the function. In the above program we return out the `sum` and hence when we get the answer out.

Its not that we must use `return` statement to return a value. The last statement that gets executed in a Ruby function gets returned by default. Consider the program link:code/function_last_gets_returned.rb[function_last_gets_returned.rb] that's shown below. In it we have a method called `square_it` which accepts a single argument called `x`. It has a single statement `x**2` which happens to be the last statement as well.

[source, ruby]
----
include::code/function_last_gets_returned.rb[]
----

Type the program and execute it. 

----
25
----

As you see we have called `square_it 5` and we get 25 as the result. Its possible because in Ruby the result of last executed statement gets returned by default.

=== Keyword arguments

To understand Keyword argument it type the program below and execute it:

[source, ruby]
----
include::code/keyword_argument.rb[]
----

Output

----
Hello Joseph your age is 7
Hello Vignesh your age is 21
Hello Martin your age is 33
----

So, to see how this feature work, lets analyze the code. Look at the function definition of say_hello , its as shown below

[source, ruby]
----
def say_hello name: "Martin", age: 33
  puts "Hello #{name} your age is #{age}"
end
----

Look at the highlighted part `def say_hello name: "Martin", age: 33`. Here we don't specify arguments as `def say_hello name= "Martin", age= 33` rather we use special `name: “Martin”`, we have taken out the equal to sign and replaced with colon. So whats the use? Now take at look at the part where the function is called

[source, ruby]
----
say_hello name: "Joseph", age: 7
say_hello age: 21, name: "Vignesh"
say_hello
----

The first line is straight forward `say_hello name: "Joseph", age: 7`, here the first argument is `name` and second argument is `age`. But look at the code `say_hello age: 21, name: "Vignesh"` , here the first argument is `age` and second one is `name`. But since the argument is hinted by the keyword, its position is irrelevant and the method prints a string as we expect.

The third line `say_hello` is just to show what happens if arguments are missed, since we have specified default values, it takes the default ones.
Is it possible to use keyword arguments with default values? Absolutely yes. Try the program below and see for yourself

[source, ruby]
----
include::code/keyword_argument_no_defaults.rb[]
----

=== Recursive function

Lets see another math thing. You might be wondering why am I doing all math? Certain programmers do write books that keeps out as much math out as possible, I am not a professional mathematician, but I admire math. Computers are based on math. All computers use a thing called Boolean algebra to do all tasks. I wouldn't say that you must be a mathematician to be a programmer, but knowing math does help.

OK what is a factorial? Take a number, lets take 3, now what will be 3 X 2 X 1, that will be six! Simple isn't it? 6 is factorial of 3. Well we will take 4 now, so 4 X 3 X 2 X 1 will be 24, in similar way factorial of 2 will be 2 X 1 which is 2. Having equipped with this knowledge we will now construct a program that will give us factorial of a number.

Study the program given below. Concentrate on the function named factorial

[source, ruby]
----
include::code/factorial.rb[]
----

Execute the code above and this is what you get as output

----
Factorial of 17 = 355687428096000 
----

In the above example (in the function factorial) we have taken all number from one to the particular number, multiplied it and got factorial. Now study the code link:code/factorial_1.rb[factorial_1.rb] shown below

[source, ruby]
----
include::code/factorial_1.rb[]
----

Execute the code above and this is what you get as output

----
Factorial of 17 = 355687428096000 
----

The output is same as the previous program link:code:factorial.rb[factorial.rb]. Take a very close look at the function named `factorial` in above program. Let me list it out for you to see

[source, ruby]
----
def factorial num
	return 1 if num == 1 
	return num * factorial(num-1)
end
----

This function is confusing, isn't it? When I was crazy and did want to learn about programming I studied C. The concept of recursive functions was explained using factorial, and I never did understand it for a long time. To avoid the pain let me explain in detail.

Take the number 1. Factorial of it is 1. So if 1 is encountered 1 is returned as shown in code below

[source, ruby]
----
def factorial num
	return 1 if num == 1 
	return num * factorial(num-1)
end
----

Now take the number 2. Factorial of it 2 X 1 , which is 2 multiplied factorial of 1. In other words we can write it as 2 multiplied by factorial of 2-1. So if number two is encountered in the function `factorial`, it skips the first if statement and the second statement `return num * factorial(num-1)` below gets executed

[source, ruby]
----
def factorial num
	return 1 if num == 1 
	return num * factorial(num-1)
end
----

In this an interesting thing happens. Here factorial (2-1) is called, that is factorial function calls itself. So when factorial of 2-1 ,  i.e factorial of 1 is called, it returns 1, this 1 is multiplied by 2 and is returned back, so in this case 2 is returned ultimately.

Now take the number 3. Its factorial is 3 X 2 X 1. This can be written as 3 multiplied by factorial 2. Factorial 2 gets translated as 2 multiplied by factorial 1. Hence the result is got out finally. For any number larger than 1, the factorial function calls itself repeatedly. The process of function calling itself is called recursion.

=== Variable number of arguments

Lets say that you do not know how many arguments are passed to a function, lets say that you are writing a function to add N numbers, the value of N is not known, so how could you get variable number of arguments. Well type the program link:code/function_variable_arguments.rb[function_variable_arguments.rb] that's given below and execute it.

[source, ruby]
----
include::code/function_variable_arguments.rb[]
----

Output

----
1 
Others are: 
2 
3 
4 
5
----
 
So the output of the program is shown above.  As you see we pass 1,2,3,4,5 as arguments, then `a` is just a single variable and hence it takes the value 1, the other variables are be absorbed by the variable `*others`  (note the star `*` before variable name) which is a special kind of argument, it takes all the rest of the arguments that are not absorbed by previous argument variables and stores it in variable name others (as an array). Now in the following piece of code

[source, ruby]
----
for x in others
	puts x
end
----

Well that's it. Now try writing a function to find maximum of n-numbers and write another function to find minimum of n-numbers and write a program to find maximum and minimum of a bunch of numbers.

=== Hashes as function arguments

Another way to sneak in multiple arguments into a function is to pass them as hashes. Look at the program below, we have a function named `some_function` which gets in two arguments, the first one named `first_arg` and second one named `others_as_hash`, we call this function in the following line `some_function "Yoda", {jedi: 100, sword: 100, seeing_future: 100}` , execute it and note the output

[source, ruby]
----
include::code/hashes_to_functions.rb[]
----

Output

----
Your first argument is: Yoda 
Other arguments are: 
{:jedi=>100, :sword=>100, :seeing_future=>100}
----

As we have expected the program prints the first argument and the hash passed to `others_as_hash`, well this one is no surprise, but take a look at the program link:code/hashes_to_function_1.rb[hashes_to_function_1.rb] below, execute it, its output will be the same as program above

[source, ruby]
----
include::code/hashes_to_functions_1.rb[]
----

But just note the this part, we have called `some_function` as shown 

[source, ruby]
----
some_function "Yoda", jedi: 100, sword: 100, seeing_future: 100
----

In the function we pass the second argument as a hash but its given as shown above, note that we have conveniently avoided the curly braces and it still works. That's the point.

