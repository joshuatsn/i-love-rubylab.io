# decorator.rb

class User
  def born_on
    Time.new(1989, 9, 10)
  end
end

class UserDecorator < SimpleDelegator
  def birth_year
    born_on.year
  end
end

decorated_user = UserDecorator.new(User.new)
puts decorated_user.birth_year
puts decorated_user.__getobj__
puts decorated_user.class
