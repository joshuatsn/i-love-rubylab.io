# singleton_example.rb

require 'singleton'

class SingletonExample
  include Singleton
end

puts SingletonExample.object_id
puts SingletonExample.object_id
