# composite.rb

class Node
  attr_accessor :name, :parent, :children

  def initialize name = "node"
    @name = name
    @parent = nil
    @children = []
  end

  def to_s
    "<Node: #{@name}, id: #{self.object_id}>"
  end

  def add_child node
    children << node
    node.parent = self
  end
end

n1 = Node.new "n1"
n2 = Node.new "n2"
n3 = Node.new "n3"

n1.add_child n2
n1.add_child n3

puts "children of #{n1} are:"
for node in n1.children
  puts node
end

puts
puts "Parent of #{n3} is #{n3.parent}"
