=== true != “true”

In the logic operator section you might see that irb gives `true` or `false` as output. You mustn't confuse with `“true”` and `“false”`. The `true` and `false` are logical values whereas `“true”` and `“false”` are `String`.

