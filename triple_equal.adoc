=== ===

The `===` operator is used to check if a particular instancefootnote:[https://en.wikipedia.org/wiki/Instance_%28computer_science%29] belongs to an class (i.e type). For example `“abc”` is a `String` type object, `1` is a `Integer`, so lets apply `===` on them and check

[source, ruby]
----
>> String === "abc"
=> true
>> Integer === 1
=> true
----

As you can see from the above examples, we have the class name on the left and the instance on the right. The first two examples are `true` since `“abc”` is String and `1` is a Integer.

[source, ruby]
----
>> String === 7
=> false
----

In the above example, it clearly returns false since `7` is no way a `String`, well thats what you might think ;)

But there is something strange, look at the example below

[source, ruby]
----
>> "abc" === String
=> false
----

So always have the class left side and instance on the right.

