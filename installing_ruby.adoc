== Installing Ruby

=== Installing Ruby on Debian flavor GNU/Linux

You need to install something called RVM (ruby version manager) which will manage multiple ruby versions. Why? It's because Ruby's version changes so fast.. Before you had 1.8, now 1.9 and soon Ruby 2 will be out. Apart from just using Ruby alone, you will also use it for other stuff like web development with packages such as Sinatra and Ruby on RailsTM . You might need to change from one version to other without uninstalling and reinstalling ruby again and again. RVM manages this for you. With simple commands we can switch between Ruby versions easily.

*Installing RVM :*

OK, to install RVM, you need to have curl (a program that can download things). To get curl, just type

[source, bash]
----
$ sudo apt-get install curl
----

Now install RVM using the following command

[source, bash]
----
$ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

$ \curl -sSL https://get.rvm.io | bash -s stable
----

Once done give these commands into terminal. These will tell Ubuntu GNU/Linux where to find the rvm.

[source, bash]
----
$ echo '[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*' >> ~/.bashrc

$ source ~/.bashrc
----

*Installing Ruby*

Once rvm is inatalled, you can install Ruby

[source, bash]
----
$ rvm install ruby
----

Once this is done, you may need to restart your terminal. Open the terminal and type the following:

----
$ ruby -v
----

It will spit an output something like this:

----
ruby 2.4.0p0 (2016-12-24 revision 57164) [x86_64-linux]
----

Then all is OK!

=== Installing IDE

You need a good IDE (Integrated development environment) to get started with Ruby. I recommend simple and light weight IDE Geany1. To install the IDE in Ubuntu just type (without that $):

[source, bash]
----
$ sudo apt-get install geany
----

If the system asks for administrator password, provide it.

